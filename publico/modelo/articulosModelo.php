<?php

require_once __DIR__ . '/../utils/conexion.php';

class ArticulosModelo extends Conexion {

    function __construct($datosConexion) {
        parent::__construct($datosConexion);
    }

    public function getAllArticulos() {
        $api = file_get_contents($this->getUrl() . "articulos");
        $articulos = json_decode($api, true);
        return $articulos;
    }

    public function getArticulosCategoria($id) {
        $api = file_get_contents($this->getUrl() . "articulos/$id");
        $articulosCategoria = json_decode($api, true);
        return $articulosCategoria;
    }
    public function getCategoriaArticulo($id) {
        $api = file_get_contents($this->getUrl() . "categorias/$id");
        $articulosCategoria = json_decode($api, true);
        return $articulosCategoria;
    }

    public function getUltimosArticulos($narticulos) {
        $api = file_get_contents($this->getUrl() . "articulos?ultArticulos=$narticulos");
        $ultimosArticulos = json_decode($api, true);
        return $ultimosArticulos;
    }

    public function getArticulo($id) {
        $api = file_get_contents($this->getUrl() . "articulos/$id");
        $articulo = json_decode($api,true);
        return $articulo;
    }

}
