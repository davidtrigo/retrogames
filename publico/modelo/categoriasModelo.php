<?php

require_once __DIR__. '/../utils/conexion.php';

class CategoriasModelo extends Conexion {

    function __construct($datosConexion) {
        parent::__construct($datosConexion);
    }

    public function getAllCategorias() {
        $api = file_get_contents($this->getUrl() . "categorias");
        $categorias = json_decode($api, true);
        return $categorias;
    }

    public function getCategoria($nombre_url) {

        $api = file_get_contents($this->getUrl() . "categorias");
        $busqueda = json_decode($api, true);
        foreach ($busqueda as $url => $id) {
            if ($id[0]['nombre_url'] === $nombre_url) {
                $categoriaElegida = $id[0]['id'];
            }
        }
        $api2 = file_get_contents($this->getUrl() . "categorias/$categoriaElegida");
        $categoria = json_decode($api2, true);
        return $categoria;
    }

}
