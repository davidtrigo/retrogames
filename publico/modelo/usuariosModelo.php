<?php

class UsuariosModelo {

    private $_conection;

    function __construct() {

        $this->_conection = new PDO("mysql:host=localhost; dbname=bbdd_retrogames;charset=utf8", 'root', '');
        $this->_conection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function conex() {
        $query = $this->_conection->query("select * from categoria");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function verificaUsuario($usuario) {
        $queryNombre = $this->_conection->query("select nombre from usuario where nombre='$usuario'");
        $usuario = $queryNombre->fetch(PDO::FETCH_ASSOC);
        return $usuario;
    }

    public function verificaPassword($password) {
        /* ==========Cifrado del password========== */
        $cifrado = hash('sha256', $password);
        $queryPassword = $this->_conection->query("select password from usuario where password='$cifrado'");
        $passwordCifrado = $queryPassword->fetch(PDO::FETCH_ASSOC);
        return $passwordCifrado;
    }

}
