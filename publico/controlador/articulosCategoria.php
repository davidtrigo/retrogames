<?php

class ArticulosCategoria {

    private $c;

    function __construct($container) {
        $this->c = $container;
    }

    public function getCategoriaArticulo($request, $response, $args) {
        $nombre_url = $args['url'];
        $categoria = $this->c->categorias->getCategoria($nombre_url);
        $datos['categoria'] = $categoria;
        $articulos = [];
        foreach ($categoria['articulo'] as $key) {
            $array = $this->c->articulos->getArticulosCategoria(\preg_replace("/[^0-9]/", "", $key));
            array_push($articulos, $array);
        }
        $datos['articulos'] = $articulos;
        if ($categoria === null) {
            $datos['msg'] = "Ha habido un error al realizar la búsqueda";
        } else {
            $response = $this->c->vista->render($response, "categorias.php", $datos);
            return $response;
        }
    }

}
