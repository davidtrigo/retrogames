<?php

class CategoriasControl {

    private $c;

    function __construct($container) {
        $this->c = $container;
    }

    public function getCategorias($request, $response, $args) {
        $categorias = $this->c->categorias->getAllCategorias();
        $datos['categorias'] = $categorias;
        $ultimosArticulos = $this->c->articulos->getUltimosArticulos(3);
        $datos['ultimosArticulos'] = $ultimosArticulos;

        if ($categorias === null) {
            $datos['msg'] = "Ha habido un error al realizar la búsqueda";
        } else {
            $response = $this->c->vista->render($response, "home.php", $datos);
            return $response;
        }
    }

}
