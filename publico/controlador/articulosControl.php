<?php

class articulosControl {

    private $c;

    function __construct($container) {
        $this->c = $container;
    }

    public function getArticulos($request, $response, $args) {
        $articulos = $this->c->articulos->getAllArticulos();
        $datos['articulos'] = $articulos;
        if ($articulos === null) {
            $datos['msg'] = "Ha habido un error al realizar la búsqueda";
        } else {
            $response = $this->c->vista->render($response, "categorias.php", $datos);
            return $response;
        }
    }

    public function getArticulo($request, $response, $args) {
        $id = $args['id'];
        $articulo = $this->c->articulos->getArticulo($id);
        
        $datos['articulo'] = $articulo;
        $categorias=[];
        //Obtener la categoría a la que pertenecen los artículos
        foreach ($articulo['categoria'] as $key) {
            $array = $this->c->articulos->getCategoriaArticulo(\preg_replace("/[^0-9]/", "", $key));
            array_push($categorias, $array[0]['nombre']);
        }
        
        $datos['categorias'] = $categorias;
        if ($articulo === null) {
            $datos['msg'] = "Ha habido un error al realizar la búsqueda";
        } else {
            $response = $this->c->vista->render($response, "articulo.php", $datos);
            return $response;
        }
    }

}
