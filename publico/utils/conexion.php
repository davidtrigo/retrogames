<?php

class Conexion{
    private $_conection;
    private $_url;
    public function __construct($datosConexion)
    {
        $this->_conection = new PDO("mysql:host={$datosConexion['host']}; dbname={$datosConexion['dbname']};charset=utf8", $datosConexion['user'], $datosConexion['password']);
        $this->_conection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_url=$datosConexion['url'];
    }
    
    public function getConexion() {
        return $this->_conection;
    }
    public function getUrl() {
        return $this->_url;
    }
    
}
