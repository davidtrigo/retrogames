<?php

//area de includes
require_once 'vendor/autoload.php';
require_once 'modelo/articulosModelo.php';
require_once 'controlador/articulosControl.php';
require_once 'modelo/categoriasModelo.php';
require_once 'controlador/categoriasControl.php';
require_once 'controlador/articulosCategoria.php';

$configuration = ['settings' => ['displayErrorDetails' => true, 'addContentLengthHeader' => false]];

$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
//Añadir el gestor de plantillas
$c = $app->getContainer();
$c['vista'] = function($c) {
    $renderer = new Slim\Views\PhpRenderer('vista');
    $renderer->addAttribute("baseURL", $c['request']->getUri()->getBasePath());
    return $renderer;
};
$db = [
    'host' => 'localhost',
    'dbname' => 'bbdd_retrogames',
    'user' => 'root',
    'password' => '',
   'url' => 'http://localhost/Retrogames/api/'
];


/*
 * $db = [
    'host' => 'retrogames.mywebcommunity.org',
    'dbname' => '3225151_retrogames',
    'user' => '3225151_retrogames',
    'password' => '6RkbTl9v8hXksqg',
   'url' => 'http://retrogames.mywebcommunity.org/Retrogames/api/'
];
 *
 * */

$c['categorias'] = function () {
    global $db;
    $categoriasModelo = new CategoriasModelo($db);
    return $categoriasModelo;
};

$c['articulos'] = function() {
    global $db;
    $articulosModelo = new ArticulosModelo($db);
    return $articulosModelo;
};
//Rutas de la app 
$app->get("/", "\categoriasControl:getCategorias");
$app->get("/categorias/{url}", "\articulosCategoria:getCategoriaArticulo");
$app->get("/articulos/{id}", "\articulosControl:getArticulo");
$app->get("/login", "\usuarionsModelo:verificaUsuario");
$app->run();
?>