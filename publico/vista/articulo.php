<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Retrogames</title>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">

    </head>
    <body>
        <!--==========Container Principal==========-->
        <div class="container">
            <header class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-4">
                        <img src="../img/logo.jpg" alt="">
                    </div>

                    <div class="col-md-8 col-sm-4 text-center">
                        <h1>RETRO GAMES</h1>
                        <h2>Los mejores juegos arcade</h2>
                    </div>

                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo $baseURL ?>/vista/login.php" id="irPrivado" class="btn btn-danger btn-lg btn-block">Zona privada</a>
                        <a href="<?php __DIR__ ?>/retrogames/doc" id="api" class="btn btn-primary btn-lg btn-block">Api Rest</a>
                    </div>
                </div><!--cierre de row-->
            </header>
            <!--========Menú de navegación==========-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="<?php echo $baseURL ?>/"  id="navbarNav1">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            </nav>
            <?php
            ?>
            <!--==========Fin de menu de navegacion==========-->
            <!--==========Breadcrumb Area==========-->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" aria-current="page">Home</li>
                    <li class="breadcrumb-item" aria-current="page"><?php echo $categorias[0] ?></li>
                    <li class="breadcrumb-item active" aria-current="page"><b><?php echo $articulo['titulo'] ?></b></li>
                </ol>
            </nav>

            <!--==========Sección de artículos==========-->

            <section>
                <h3>Lista de Artículos</h3><br>
                <?php
                echo ' <div class="card-columns">';
                echo '<article class="card text-center">';
                echo '<img class="img-thumbnail" src="../img/articulos/' . $articulo['foto'] . '" alt="' . $articulo['foto'] . '">';
                echo '<div class="card-body">';
                echo '<h2 class="card-title">' . $articulo['titulo'] . '</h2>';
                echo '<p class="card-text"><br>' . $articulo['contenido'] . '</p>';
                echo '<p class="card-text"> <b>Artículo creado el </b>' . $articulo['fecha_creacion'] . '</p>';
                echo '</div>';
                echo'</article>';
                echo '</div>';
                ?>
            </section>
            <!--==========Fin de Sección de Categorías==========-->
            <footer id="peu" class="container-fluid text-center">
                <a href="<?php echo $baseURL ?>/">Web pública</a> |
                <a href="<?php __DIR__ ?>/retrogames/doc">API REST</a> |
                <a href="<?php echo $baseURL ?>/vista/login.php">Zona privada</a>
            </footer>
        </div>
    </body>
</html>