<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Retrogames</title>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">

    </head>
    <body>
        <!--==========Container Principal==========-->
        <div class="container">
            <header class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-4">
                        <img src="../img/logo.jpg" alt="logo">
                    </div>

                    <div class="col-md-8 col-sm-4 text-center">
                        <h1>RETRO GAMES</h1>
                        <h2>Los mejores juegos arcade</h2>
                    </div>

                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo $baseURL ?>/vista/login.php" id="irPrivado" class="btn btn-danger btn-lg btn-block">Zona privada</a>
                        <a href="<?php __DIR__ ?>/retrogames/doc" id="api" class="btn btn-primary btn-lg btn-block">Api Rest</a>
                    </div>
                </div><!--cierre de row-->
            </header>
            <!--========Menú de navegación==========-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="<?php echo $baseURL ?>/"  id="navbarNav1">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
            <?php
            ?>
            <!--==========Fin de menu de navegacion==========-->
            <!--==========Breadcrumb Area==========-->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page"><b><?php echo $categoria[0]['nombre'] ?></b> </li>
                </ol>
            </nav>
            <!--==========Sección de categorías==========-->
            <h1><?php echo $categoria[0]['nombre'] ?></h1>
            <blockquote><?php echo $categoria[0]['descripcion'] ?></blockquote>
            <!--==========Sección de artículos==========-->

            <section>
                <h2 class="display-6 text-center">Artículos de la categoría</h2><br>
                <?php
                echo ' <div class="card-columns">';
                foreach ($articulos as $key => $value) {
                    echo '<article class="card text-center">';
                    echo '<img class="img-thumbnail" src="../img/articulos/' . $value['foto'] . '" alt="' . $value['foto'] . '">';
                    echo '<div class="card-body">';
                    echo '<h5 class="card-title">' . $value['titulo'] . '</h5>';
                    echo '<p class="card-text col-30 text-truncate">' . $value['contenido'] . '</p>';
                    echo '<a href="' . $baseURL . '/articulos/' . $value['id'] . '" class="btn btn-primary">Ver artículo</a>';
                    echo '</div>';
                    echo'</article>';
                }
                echo '</div>';
                ?>
            </section>
            <!--==========Fin de Sección de Categorías==========-->
            <footer id="peu" class="container-fluid text-center">
                <a href="<?php echo $baseURL ?>/">Web pública</a> |
                <a href="<?php __DIR__ ?>/retrogames/doc">API REST</a> |
                <a href="<?php echo $baseURL ?>/vista/login.php">Zona privada</a>
            </footer>
        </div>
    </body>
</html>