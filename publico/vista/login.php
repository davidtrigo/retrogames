<!DOCTYPE html>
<html>
    <head>
        <title>Retrogames</title>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <!--==========Container Principal==========-->
        <div class="container">
            <header class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-4">
                        <img src="../img/logo.jpg" alt="logo">
                    </div>
                    <div class="col-md-8 col-sm-4 text-center">
                        <h1>RETRO GAMES</h1>
                        <h2>Los mejores juegos arcade</h2>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <a href="<?php __DIR__ ?>../" id="irPrivado" class="btn btn-danger btn-lg btn-block">Web pública</a>
                        <a href="<?php __DIR__ ?>/retrogames/doc" id="api" class="btn btn-primary btn-lg btn-block">Api Rest</a>
                    </div>
                </div><!--cierre de row-->
            </header>
            <!--========Menú de navegación==========-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="<?php __DIR__ ?>/retrogames/publico/"  id="navbarNav">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
            <?php
            ?>
            <!--==========Fin de menu de navegacion==========-->
            <!--==========Breadcrumb Area==========-->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Login</b></li>
                </ol>
            </nav>
            <!--==========Footer==========-->
            <div class="row justify-content-center" id="login">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Login</h5>
                            <!--==========Formulario==========-->
                            <form action="login.php" method="POST">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" name="user" placeholder="Usuario" aria-describedby="sizing-addon1" required>
                                </div>
                                <br>
                                <div class="input-group input-group-lg">
                                    <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="sizing-addon1">
                                </div>
                                <br>
                                <input type="submit" name="submit" class="btn btn-primary" value="submit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--==========Footer==========-->
            <footer id="peu" class="container-fluid text-center">
                <a href="<?php __DIR__ ?>/retrogames/publico/">Web pública</a> |
                <a href="<?php __DIR__ ?>/retrogames/doc/">API REST</a> |
                <a href="<?php __DIR__ ?>/retrogames/privado">Zona privada</a>
            </footer>
        </div>
        <?php
        require_once '../modelo/usuariosModelo.php';
        $conex = new UsuariosModelo();
        if (isset($_POST['submit'])) {
            $user = $_POST['user'];
            $password = $_POST['password'];
            $exito = false;
            //Verificar Usuario
            $verifica = $conex->verificaUsuario($user);
            if ($verifica) {
                echo '<h4 class="message"> El usuario es correcto </h4>';
            } else {
                echo '<h4 class="message"> El usuario no es correcto </h4>';
            }
            //Verifica Password
            $verifica2 = $conex->verificaPassword($password);
            if ($verifica2) {
                echo '<h4 class="message"> El password es correcto </h4>';
            } else {
                echo '<h4 class="message"> El password no es correcto </h4>';
            }
            if ($verifica && $verifica2) {
                header("Location: http://localhost/retrogames/privado/");
                session_start();
            } else {
                header("Location: http://localhost/retrogames/publico/vista/login.php");
            }
        }
        ?>
    </body>
</html>