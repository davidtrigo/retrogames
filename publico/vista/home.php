<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Retrogames</title>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
        <!--==========Container Principal==========-->
        <div class="container">
            <header class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-4">
                        <img src="img/logo.jpg" alt="logo">
                    </div>

                    <div class="col-md-8 col-sm-4 text-center">
                        <h1>RETRO GAMES</h1>
                        <h2>Los mejores juegos arcade</h2>
                    </div>

                    <div class="col-md-2 col-sm-4">
                        <a href="<?php __DIR__ ?>./vista/login.php" id="irPrivado" class="btn btn-danger btn-lg btn-block">Zona privada</a>
                        <a href="<?php __DIR__ ?>/retrogames/doc" id="api" class="btn btn-primary btn-lg btn-block">Api Rest</a>
                    </div>
                </div><!--cierre de row-->
            </header>
            <!--========Menú de navegación==========-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="<?php echo $baseURL ?>/"  id="navbarNav1">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                        <!--==========Fin del código para cargar el menú==========-->


            </nav>
            <!--==========Fin de menu de navegacion==========-->
            <!--==========Breadcrumb Area==========-->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page"><b>Home</li></b></li>
                </ol>
            </nav>

            <!--==========Sección de categorías==========-->
            <section>
                <h3>Lista de Categorías</h3><br>
                <?php
                echo '<div class = "card-columns">';
                foreach ($categorias as $categoria => $value) {
                    echo '<article class = "card text-center">';
                    echo '<img class = "img-thumbnail" src = "img/categorias/' . $value[0]['foto'] . '" alt = "' . $value[0]['foto'] . '">';
                    echo '<div class = "card-body">';
                    echo '<h5 class = "card-title">' . $value[0]['nombre'] . '</h5>';
                    echo '<p class = "card-text col-20 text-truncate">' . $value[0]['descripcion'] . '</p>';
                    echo '<a href="categorias/' . $value[0]['nombre_url'] . '" class="btn btn-primary">Ver categoría</a>';
                    echo '</div>';
                    echo '</article>';
                }
                echo '</div>';
                ?>
            </section>

            <!--==========Fin de Sección de Categorías==========-->
            <!--Sección de últimos artículos-->
            <section>
                <h2 class="display-6 text-center">Últimos Articulos</h2><br>
                <?php
                echo '<div class = "card-columns">';
                foreach ($ultimosArticulos as $articulo => $value) {
                    echo '<article class = "card text-center">';
                    echo '<img class = "img-thumbnail" src = "img/articulos/' . $value['foto'] . '" alt = "' . $value['foto'] . '">';
                    echo '<div class = "card-body">';
                    echo '<h5 class = "card-title">' . $value['titulo'] . '</h5>';
                    echo '<a href="articulos/' . $value['id'] . '" class="btn btn-primary">Ver artículo</a>';
                    echo '</div>';
                    echo '</article>';
                }
                echo '</div>';
                ?>
            </section>
            <footer id="peu" class="container-fluid text-center">
                <a href="<?php echo $baseURL ?>/">Web pública</a> |
                <a href="<?php __DIR__ ?>/retrogames/doc">API REST</a> |
                <a href="<?php __DIR__ ?>./vista/login.php">Zona privada</a>
            </footer>
        </div>
    </body>
</html>