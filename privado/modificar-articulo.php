<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $('tr').click(function(){
            var texto = $(this).closest('tr').children('td.celda1').text();
            console.log(texto);
            texto = "index.php?app=modificar-articulo&ID="+texto;
            console.log(texto);
            location.href= texto;

        });

        $('#cTitulo').change(function(){
            var titulo = $('#cTitulo').value;
            titulo.replace(" ","-");
            $('#cTitulo_url').value(titulo);
        });

        $(function() {
            $('#cFoto').change(function(e) {
                addImage(e);
            });

            function addImage(e){
                var file = e.target.files[0],
                imageType = /image.*/;

                if (!file.type.match(imageType)) {
                    return;
                }

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(e) {
                var result=e.target.result;
                $('#imgSalida').attr("src",result);
            }
        });

    });
</script>

<?php

/*
La function modificar-articulo()
--------------------------------
Modifica un artículo de la base de datos de juegos (bbdd_retrogames).
*/

require_once "funciones-comunes.php";

function modificarArticulo(){
echo <<<form_modifica
    <h3 class="mensaje_central">Modificar un artículo</h3>
    <form class="formulario" name="form1" action="" method="post">
        <div class="input-group">
            <span class="input-group-addon"><b>Buscar artículo/s</b></span>
            <input id="cTitulo" type="text" class="form-control" name="cTitulo" value="">
            <input type="submit" value=" Enviar " id="enviar" name="enviar" class="btn btn-success" />
        </div>
    </form>
form_modifica;

    if (isset($_POST['enviar'])){
        $busca = $_POST['cTitulo'];
    } else {
        $busca = "*";
    }
    $lista = buscar_articulos($busca);

    if (count($lista) > 0){
        echo "<div class='formulario'>";
        echo "<p><b>Artículos encontrados: ".count($lista). "</b></p>";
        echo "<div id='form_modificar'>";
        echo "<table class='table table-condensed'>";
        echo "<tr><th> ID </th> <th> Título </th></tr>";
        foreach ($lista as $fila) {
                echo "<tr><td class='celda1'>{$fila['id']}</td> <td>{$fila['titulo']}</td></tr>";
        }
        echo "</table>";
        echo "</div>";
        echo "</div>";

    } else {

    }
    if (isset($_GET['ID'])) {
        $idSeleccionado= $_GET['ID'];
        require_once "modifica-datos-articulo.php";
        modifArticulo($idSeleccionado);
    }

}
?>
