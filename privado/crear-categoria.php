        <!-- Scrip que carga y visualiza la imangen del juego  -->
        <script type="text/javascript" language="javascript">
        $(document).ready(function(){

            $('#imgSalida').attr("src","./img/image-not-found.png");

            $(function() {
                $('#cFoto').change(function(e) {
                    addImage(e);
                });

                function addImage(e){
                    var file = e.target.files[0],
                    imageType = /image.*/;

                    if (!file.type.match(imageType)) {
                        return;
                    }

                    var reader = new FileReader();
                    reader.onload = fileOnload;
                    reader.readAsDataURL(file);
                }

                function fileOnload(e) {
                    var result=e.target.result;
                    $('#imgSalida').attr("src",result);
                }
            });

            $(".Titulo").change(function(){
                var texto=$(this).val().replace(/^ | $/g, '');
                texto=texto.toLowerCase();
                texto=texto.replace(/á|à/g, 'a');
                texto=texto.replace(/é|è/g, 'e');
                texto=texto.replace(/í|ï/g, 'i');
                texto=texto.replace(/ó|ò/g, 'o');
                texto=texto.replace(/ú|ü/g, 'u');
                texto=texto.replace(/ñ/g, 'nn');
                texto=texto.replace(/ /g, '-');
                texto=texto.replace(/--|---|---/g, '-');
                $(".Titulo_url").attr("value", texto);
            });
        });
        </script>

<?php

/*
La function crear-articulo()
--------------------------------
Crea un nuevo artículo en la base de datos de juegos (bbdd_retrogames)
*/
require_once "funciones-comunes.php";

function crearCategoria(){
echo <<<codigoHTML
    <section class="Seccion">
        <h3 class="mensaje_central">Introducir nueva categoría</h3>
        <form class="formulario" action="" method="post" enctype="multipart/form-data" autocomplete="off" id="form_crear_cat">
            <div class="input-group">
                <span class="input-group-addon">Nombre</span>
                <input id="cNombre" type="text" class="form-control Titulo" name="cNombre" required>
            </div>
            <div class="input-group">
                <span class="input-group-addon">Nombre URL</span>
                <input id="cNombre_url" type="text" class="form-control Titulo_url" name="cNombre_url">
            </div><hr color="greenyellow" required>
            <div class="input-group">
                <span class="input-group-addon"><img id="imgSalida" src="" alt="Foto"/></span>
                <input id="cFoto" type="file" class="form-control Foto" name="cFoto")>
            </div><br>
            <div class="input-group">
                <span class="input-group-addon">Descripción</span>
                <textarea cols="50" rows="2" form="form_crear_cat"  id="cDescripcion" type="text" class="form-control Contenido" name="cDescripcion" placeholder="La esquena inferior derecha para apliar">
            </div>
            <hr color="greenyellow">
            <div class="input-group">
                <span class="input-group-addon"> </span>
                <button type="submit" class="btn btn-success">CREAR CATEGORÍA</button>
            </div>
        </form>
codigoHTML;

        // --- Si existe la Foto, la sube a la carpeta pública de imagenes de categorías ------
        if ( isset($_FILES['cFoto']['name']) ){

            $ruta_indexphp = dirname(realpath(__FILE__));
            $ruta_fichero_origen = $_FILES['cFoto']['tmp_name'];
            $ruta_nuevo_destino = '../publico/img/categorias/' . $_FILES['cFoto']['name'];
            if( move_uploaded_file ( $ruta_fichero_origen, $ruta_nuevo_destino ) ) {
                echo 'Foto guardada con éxito';
            } else {
                    echo "<p class='error'>El fichero [{$_FILES['cFoto']['name']}] no se ha podido subir </p>";
            }
        }


        // INTRODUCCION DE DATOS EN TABLA ARTICULOS
        if ($_POST != null){

            try{
                $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
            } catch (PDOException $e) {
                echo "<p class='KO'>
                {$e->getMessage()} </p>";
            }

            // CONECTAR CON LA BASE DE DATOS
            $sql = "USE bbdd_retrogames;";
            $OK = $conexion->exec($sql);


            if($OK===false) {
                $msg_titulo= "Error de conexión";
                $msg_1= "<p class='KO'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> no establecida</p>";
                $msg_2= "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";

            } else {
                //echo "<p class='OK'>Conexión con la BBDD <b>bbdd_retrogames</b> establecida.</p>";
                $sql2 = $conexion->prepare("INSERT INTO categoria (nombre, nombre_url, foto, descripcion) VALUES('{$_POST["cNombre"]}', '{$_POST["cNombre_url"]}', '{$_FILES["cFoto"]["name"]}', '{$_POST["cDescripcion"]}');");

                $OK = $sql2->execute();
                if($OK===false) {
                    $msg_titulo= "Error de registro de datos";
                    $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
                    $msg_2= "<p class='KO'>No se ha podido registrar los datos de la categoría en <b>bbdd_retrogames</b></p>";

                } else {
                    $msg_titulo= "Categoría creada";
                    $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
                    $msg_2= "<p class='OK'>Datos de la categoría registrados correctamente en <b>bbdd_retrogames</b></p>";
                }
            }
            echo <<<codigoHTML2
            <div class="card formulario elevar_ventana" style="width: 18rem;">
        <!--      <img class="card-img-top" src="..." alt="Card image cap">  -->
              <div class="card-body">
                <h5 class="card-title">$msg_titulo</h5>
                <p class="card-text">$msg_1 $msg_2</p>
                <a href="index.php" class="btn btn-success boton_cerrar">Cerrar</a>
              </div>
            </div>
codigoHTML2;
        }
    echo "</section> <br /><br />";
}
?>
    </body>
</html>
