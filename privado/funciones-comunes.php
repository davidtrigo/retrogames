<?php
    // ----------- Función que devuelve un array con todas las categorías ------
    function lista_categorias() {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LAS CATEGORIAS -----
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT c.id, c.nombre FROM categoria c";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }

    // ----------- Función que devuelve un array con todos los artículos -------
    function lista_articulos() {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LOS ARTÍCULOS ------
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT a.id, a.titulo, a.titulo_url, a.foto, a.contenido, a.categoria FROM articulo a";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }

    // ----------- Función que devuelve un array con las categorias según palabra entrada -------
    function buscar_categorias($palabra) {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LAS CATEGORIAS ------
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT c.* FROM categoria c WHERE c.nombre LIKE '%$palabra%'";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }


    // ----------- Función que devuelve un array con las categorias según palabra entrada -------
    function buscar_categoria_ID($IDx) {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LAS CATEGORÍAS ------
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT c.* FROM categoria c WHERE c.id like $IDx";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }


    // ----------- Función que devuelve un array con los artículos según palabra entrada -------
    function buscar_articulos($palabra) {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LOS ARTÍCULOS ------
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT a.* FROM articulo a WHERE a.titulo LIKE '%$palabra%'";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }

    // ----------- Función que devuelve un array con los artículos según palabra entrada -------
    function buscar_articulo_ID($IDx) {
        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // ------ CONECTAR CON LA BASE DE DATOS PARA LISTAR LOS ARTÍCULOS ------
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);
        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            return "Se ha producido un error.";
        } else {
            $res = "SELECT a.* FROM articulo a WHERE a.id like $IDx";
            $sql = $conexion->query($res);
            $datos = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $datos;
        }
    }

 ?>
