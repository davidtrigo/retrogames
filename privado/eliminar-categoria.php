<link rel="stylesheet" href="css/estilos.css" />

<?php

/*
La function elimina-articulo()
--------------------------------
Elimina un artículo de la base de datos de juegos (bbdd_retrogames).
*/
require_once "funciones-comunes.php";

function eliminarCategoria(){
echo <<<form_elimina
    <h3 class="mensaje_central">Eliminar una categoría</h3>
    <form class="formulario" action="" method="post">

        <div class="input-group">
            <span class="input-group-addon">Buscar categoría</span>
            <select id="xCategoria" class="form-control" name="xCategoria" required>
        </div>
            <option value=0 selected="selected"></option>
form_elimina;
    $lista = lista_categorias();
    foreach ($lista as $reg) {
        echo "<option value={$reg['id']}>{$reg['nombre']}</option>";
    }
    echo "</select> <br />";
    echo "<button type='submit' class='btn btn-success'>Eliminar categoría</button>";
    echo "</form>";

    if (isset($_POST['xCategoria'])){

        try{
            $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
        } catch (PDOException $e) {
            echo "<p class='KO'>
            {$e->getMessage()} </p>";
        }

        // CONECTAR CON LA BASE DE DATOS
        $sql = "USE bbdd_retrogames;";
        $OK = $conexion->exec($sql);

        echo "<div class='formulario'>";

        if($OK===false) {
            echo "<p class='KO'>Conexión con <b>bbdd_retrogames</b> no establecida</p>";
            echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            // errorInfo()[2] = La posición 2 del array de "errorInfo", nos da la descripción del error producido.
        } else {
            echo "<p class='OK'>Conexión con <b>bbdd_retrogames</b> establecida.</p>";
        }
        if ($_POST['xCategoria'] != null ){
            // ELIMINAR REGISTRO DE LA TABLA CATEGORIA
            $registro = $_POST['xCategoria'];
            $sql = "DELETE FROM categoria WHERE id LIKE $registro";
            $OK = $conexion->exec($sql);

            if($OK===false) {
                echo "<p class='KO'>No se ha podido registrar los datos de la categoría en <b>bbdd_retrogames</b></p>";
                echo "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";
            } else {
                echo "<p class='OK'>La categoría {$registro} ha sido eliminada de <b>bbdd_retrogames</b></p>";
                ?>
                    <input type="button" value="Aceptar"  class='btn btn-success boton_cerrar' onClick="parent.location='index.php'">
                <?php
            }
        }
        echo "</div>";
    }
}
?>
