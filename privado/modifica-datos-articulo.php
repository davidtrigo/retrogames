<!-- Scrip que carga y visualiza la imangen del juego  -->
<script type="text/javascript" language="javascript">

    function formateaTexto(valor) {
        var texto= valor.val().replace(/^ | $/g, '');
        texto=texto.toLowerCase();
        texto=texto.replace(/á|à/g, 'a');
        texto=texto.replace(/é|è/g, 'e');
        texto=texto.replace(/í|ï/g, 'i');
        texto=texto.replace(/ó|ò/g, 'o');
        texto=texto.replace(/ú|ü/g, 'u');
        texto=texto.replace(/ñ/g, 'nn');
        texto=texto.replace(/ /g, '-');
        texto=texto.replace(/--|---|---/g, '-');

        return texto;
    }

    $(document).ready(function(){

        $(".Titulo").change(function(){
            var texto = formateaTexto($(this));
            $(".Titulo_url").attr("value", texto);
        });
    //    $('#imgSalida').attr("src","./img/image-not-found.png");

        $(function() {
            $('#cFoto').change(function(e) {
                addImage(e);
            });

            function addImage(e){
                var file = e.target.files[0],
                imageType = /image.*/;

                if (!file.type.match(imageType)) {
                    return;
                }

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(e) {
                var result=e.target.result;
                $('#imgSalida').attr("src",result);
            }
        });

    });
</script>

<?php

/*
La function crear-articulo()
--------------------------------
Crea un nuevo artículo en la base de datos de juegos (bbdd_retrogames)
*/
require_once "funciones-comunes.php";

function modifArticulo($idArt){
    $datos = buscar_articulo_ID($idArt);
    $datos_art=$datos[0];

echo <<<codigoHTML
    <section class="Seccion">
        <form class="formulario elevar_ventana" action="" method="post" autocomplete="off" id="form_crear_art">
            <div class="input-group">
                <span class="input-group-addon">Título</span>
                <input id="cTitulo" type="text" class="form-control Titulo" name="cTitulo" required value="{$datos_art['titulo']}" />
            </div>
            <div class="input-group">
                <span class="input-group-addon">Titulo URL</span>
                <input id="cTitulo_url" type="text" class="form-control Titulo_url" name="cTitulo_url" required value="{$datos_art['titulo_url']}" />
            </div><hr color="greenyellow">
            <div class="input-group">
                <span class="input-group-addon">Imagen</span>
                <input id="cNombreFoto" type="text" class="form-control Foto" name="cNombreFoto" disabled value="{$datos_art['foto']}" />
                <div class="input-group">
                </div>
                <span class="input-group-addon"><img id="imgSalida" src="../publico/img/articulos/{$datos_art['foto']}"  alt="Foto"/></span>
                <input id="cFoto" type="file" class="form-control Foto" name="cFoto" accept="image/*" value="{$datos_art['foto']}" />
            </div><br>
            <div class="input-group">
                <span class="input-group-addon">Contenido</span>
                <textarea cols="50" rows="2" form="form_crear_art"  id="cContenido" type="text" class="form-control Contenido" name="cContenido" placeholder="La esquena inferior derecha para apliar">{$datos_art['contenido']}</textarea>

            </div><br>
            <div class="input-group">
                <span class="input-group-addon">Categoría</span>
                <select id="nCategoria_id" class="form-control nCategoria_id" name="nCategoria_id">
codigoHTML;
                $datos = lista_categorias();
                foreach ($datos as $fila) {
                    if ($fila['id']==$datos_art['categoria']) {
                        echo "<option value={$fila['id']} selected='selected'>{$fila['nombre']}</option>";
                    } else {
                        echo "<option value={$fila['id']}>{$fila['nombre']}</option>";
                    }
                }

echo <<<codigoHTML2
                </select><br>
            </div>
            <hr color="greenyellow">
            <div class="input-group">
                <span class="input-group-addon"> </span>
                <button type="submit" class="btn btn-success">MODIFICAR ARTÍCULO</button>
            </div>
        </form>
codigoHTML2;


        // --- Si existe la Foto, la sube a la carpeta pública de imagenes de artículos ------
        if ( isset($_FILES['cFoto']['name']) ){

            $ruta_indexphp = dirname(realpath(__FILE__));
            $ruta_fichero_origen = $_FILES['cFoto']['tmp_name'];
            $ruta_nuevo_destino = '../publico/img/articulos/' . $_FILES['cFoto']['name'];
            if( move_uploaded_file ( $ruta_fichero_origen, $ruta_nuevo_destino ) ) {
                echo 'Foto guardada con éxito';
            } else {
                    echo "<p class='error'>El fichero [{$_FILES['cFoto']['name']}] no se ha podido subir </p>";
            }
        }

        // MODIFICA LOS DATOS EN TABLA ARTICULOS
        if ($_POST != null){
            try{
                $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
            } catch (PDOException $e) {
                echo "<p class='KO'>
                {$e->getMessage()} </p>";
            }

            // CONECTAR CON LA BASE DE DATOS
            $sql = "USE bbdd_retrogames;";
            $OK = $conexion->exec($sql);


            if($OK===false) {
                $msg_titulo= "Error de conexión";
                $msg_1= "<p class='KO'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> no establecida</p>";
                $msg_2= "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";

            } else {
                //echo "<p class='OK'>Conexión con la BBDD <b>bbdd_retrogames</b> establecida.</p>";
                $sql = $conexion->prepare("UPDATE articulo SET titulo='{$_POST["cTitulo"]}', titulo_url='{$_POST["cTitulo_url"]}', foto='{$_POST["cFoto"]}', contenido='{$_POST["cContenido"]}', categoria={$_POST['nCategoria_id']}  WHERE id={$datos_art['id']} ; ");

                $OK = $sql->execute();
                if($OK===false) {
                    $msg_titulo= "Error de registro de datos";
                    $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
                    $msg_2= "<p class='KO'>No se ha podido registrar los datos del artículo en <b>bbdd_retrogames</b></p>";

                } else {
                    $msg_titulo= "Artículo modificado";
                    $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
                    $msg_2= "<p class='OK'>Datos del juego registrados correctamente en <b>bbdd_retrogames</b></p>";
                }
            }
            echo <<<codigoHTML2
            <div class="card formulario elevar_ventana" style="width: 18rem;">
              <div class="card-body">
                <h5 class="card-title">$msg_titulo</h5>
                <p class="card-text">$msg_1 $msg_2</p>
                <a href="index.php" class="btn btn-success boton_cerrar">Cerrar</a>
              </div>
            </div>
codigoHTML2;
        }
    echo "</section> <br /><br />";
}
?>
