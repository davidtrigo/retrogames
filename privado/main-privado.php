<?php
/*
ZONA PRIVADA
============
Será la zona desde la que se gestionará todo el contenido del sitio web. Se tiene que poder:
• Crear nuevas categorías.
• Eliminar categorías (que no tengan artículos asociados).
• Crear artículos.
• Eliminar artículos.
• Modificar artículos.
• Subir artículos a través de un archivo XML.
Para poder acceder a esta zona habrá que ser usuario registrado.
El editor utilizado para crear los artículos puede ser simplemente un <textarea> de HTML,
donde el creador de contenido tendrá que escribir código HTML.
*/

    echo "<main>";


    if (isset($_GET['app'])) {

        switch ($_GET['app']) {

            case "ver-categorias":
                require_once "ver-categorias.php";
                verCategorias();
                break;

            case "ver-articulos":
                require_once "ver-articulos.php";
                verArticulos();
                break;

            case "crear-categoria":
                require_once "crear-categoria.php";
                crearCategoria();
                break;

            case "eliminar-categoria":
                require_once "eliminar-categoria.php";
                eliminarCategoria();
                break;

            case "crear-articulo":
                require_once "crear-articulo.php";
                crearArticulo();
                break;

            case "modificar-articulo":
                require_once "modificar-articulo.php";
                modificarArticulo();
                break;

            case "eliminar-articulo":
                require_once "eliminar-articulo.php";
                eliminarArticulo();
                break;

            case "subir-articulo":
                include_once "subir-articulo.php";
                subirArticulo();
                break;
        }
    } else {
        echo "<h3 class='text-center mensaje_central'>Escoge una opción del menú.</h3>";
    }

    echo "</main>";

?>
