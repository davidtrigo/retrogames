<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $('tr').click(function(){
            var texto = $(this).closest('tr').children('td.celda1').text();
            texto = "index.php?app=ver-categorias&ID="+texto;
            location.href= texto;
        });
    });
</script>

<?php
require_once "funciones-comunes.php";

function verCategorias(){
echo <<<form_verCat
    <h3 class="mensaje_central">Lista de categorías</h3>
    <form class="formulario" name="form2" action="" method="post">
        <div class="input-group">
            <span class="input-group-addon"><b>Buscar categoría/s</b></span>
            <input id="cNombre" type="text" class="form-control" name="cNombre" value="">
            <input type="submit" value=" Enviar " id="enviar" name="enviar" class="btn btn-success"/>
        </div>
    </form>
form_verCat;

    if (isset($_POST['enviar'])){
        $busca = $_POST['cNombre'];
    } else {
        $busca = "*";
    }
    $lista = buscar_categorias($busca);

    if (count($lista) > 0){
        echo "<div class='formulario'>";
        echo "<p><b>Categorías encontradas: ".count($lista). "</b></p>";
        echo "<div id='form_modificar'>";
            echo "<table class='table table-condensed'>";
                echo "<tr><th> ID </th> <th> Nombre </th></tr>";
                    foreach ($lista as $fila) {
                        echo "<tr><td class='celda1'>{$fila['id']}</td><td>{$fila['nombre']}</td></tr>";
                    }
                echo "</table>";
            echo "</div>";
        echo "</div>";
    }

    if (isset($_GET['ID'])) {
        $idSeleccionado= $_GET['ID'];

        require_once "funciones-comunes.php";

            $datosx = buscar_categoria_ID($idSeleccionado);
            $datos_cat=$datosx[0];

            echo <<<codigoHTML
            <div class='formulario '>
                <div class='marcoCaja container'>
                    <input type='hidden' name='app' value='ver-categorias' />
                    <div class='row'>
                        <div class='col-8 Titulo'>Nombre: {$datos_cat['nombre']}</div>
                        <div class='col-4'><div class='col-12 Titulo'> </div>
                    </div>

                    <div class='row'>
                        <div class='col-8'>

                            <div class='row'>
                                <div class='col-12 nombre_url'>Nombre_url: {$datos_cat['nombre_url']}</div>
                            </div>

                            <div class='row'>
                                <div class='col-12 Foto'>Foto: {$datos_cat['foto']}</div>
                            </div>

                            <div class='row'>
                                <div class='col-12 Contenido'> <b>Descripción: </b> {$datos_cat['descripcion']}</div>
                            </div>

                        </div>    <!--- Cierrer de <div class='col-8'>  -->

                        <div class='col-4 imgSalida_lg' style="background-image:url('../publico/img/categorias/{$datos_cat['foto']}')">

                        </div>
                    </div>
                </div>
            </div>
codigoHTML;

        }
    }
?>
