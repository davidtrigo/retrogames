<!doctype html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>RETRO GAMES</title>

    <link rel="icon" type="image/png" href="./img/favicon_16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="./img/favicon_32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./img/favicon_64.png" sizes="64x64" />

    <link rel="stylesheet" href="./css/estilos.css">
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


  </head>
  <body>

    <div class="panel_centrado">
        <?php include_once "cap-i-peu/header.html" ?>
        <?php include_once "menu.php" ?>
        <div class="main_privado">
            <?php include_once "main-privado.php" ?>
        </div>
        <?php include_once "cap-i-peu/footer.html" ?>
    </div>
  </body>
</html>
