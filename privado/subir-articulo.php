<!-- Scrip que carga y visualiza la imangen del juego  -->
<script type="text/javascript" language="javascript">
    $(document).ready(function(){

        $('#imgSalida_lg').attr("src","./img/image-not-found.png");

        $(function() {
            $('#cFoto').change(function(e) {
                addImage(e);
            });

            function addImage(e){
                var file = e.target.files[0],
                imageType = /image.*/;

                if (!file.type.match(imageType)) {
                    return;
                }

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(e) {
                var result=e.target.result;
                $('#imgSalida_lg').attr("src",result);
            }
        });
        $(".Titulo").change(function(){
            var texto=$(this).val().replace(/^ | $/g, '');
            texto=texto.toLowerCase();
            texto=texto.replace(/á|à/g, 'a');
            texto=texto.replace(/é|è/g, 'e');
            texto=texto.replace(/í|ï/g, 'i');
            texto=texto.replace(/ó|ò/g, 'o');
            texto=texto.replace(/ú|ü/g, 'u');
            texto=texto.replace(/ñ/g, 'nn');
            texto=texto.replace(/ /g, '-');
            texto=texto.replace(/--|---|---/g, '-');
            $(".Titulo_url").attr("value", texto);
        });
    });
</script>

<?php

    /*
    La function grabar_datos() :
    ----------------------------
    Añade un el artículo subido mediante un fichero XML, integrandolo a la base de datos de juegos (bbdd_retrogames).
    */

function grabar_datos($cTitulo, $cTitulo_url, $cFoto, $cContenido){
    try{
        $conexion = new PDO ("mysql:host=localhost; charset=utf8", "root", "");
    } catch (PDOException $e) {
        echo "<p class='KO'> {$e->getMessage()} </p>";
    }

    // CONECTAR CON LA BASE DE DATOS
    $sql = "USE bbdd_retrogames;";
    $OK = $conexion->exec($sql);

    if($OK===false) {
        $msg_titulo= "Error de conexión";
        $msg_1= "<p class='KO'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> no establecida</p>";
        $msg_2= "<p class='KO'>" .$conexion->errorInfo()[2]. "</p>";

    } else {
        $sql = $conexion->prepare("INSERT INTO articulo (titulo, titulo_url, foto, contenido) VALUES('$cTitulo', '$cTitulo_url', '$cFoto', '$cContenido')");
        $OK = $sql->execute();

        if($OK===false) {
            $msg_titulo= "Error de registro de datos";
            $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
            $msg_2= "<p class='KO'>No se ha podido registrar los datos del artículo en <b>bbdd_retrogames</b></p>";

        } else {
            $msg_titulo= "Artículo creado";
            $msg_1= "<p class='OK'>BASE DE DATOS: Conexión a <b>bbdd_retrogames</b> establecida</p>";
            $msg_2= "<p class='OK'>Datos del juego registrados correctamente en <b>bbdd_retrogames</b></p>";
        }
    }
    echo <<<codigoHTML2
    <div class="card formulario elevar_ventana" style="width: 18rem;">
<!--    <img class="card-img-top" src="..." alt="Card image cap">  -->
        <div class="card-body">
            <h5 class="card-title">$msg_titulo</h5>
            <p class="card-text">$msg_1 $msg_2</p>
            <a href="index.php" class="btn btn-success boton_cerrar">Cerrar</a>
        </div>
    </div>
codigoHTML2;
}



/*
    La function subirArticulo() :
    --------------------------
    Añade un nuevo juego atrebés de un fichero XML, integrandolo a la base de datos de juegos (bbdd_retrogames).
*/

function subirArticulo(){
    echo <<<codigoHTML
    <section class='marcoFormulario'>
        <h3 class="mensaje_central">Subir nuevo artículo</h3>

        <form class="formulario" method="post" enctype="multipart/form-data">
            <label for="subir"><b>Subir un nuevo artículo (fichero en formato XML) </b></label><br>
            <div class="input-group">
                <input type="file" class="btn btn-secondary" name="subir" id="subir" value="" accept="text/xml" />
                <button type="submit"" class="btn btn-success">Ver articulo</button>
            </div>
        </form>
    </section>
codigoHTML;

    if ( isset($_FILES['subir']['name']) ){
        move_uploaded_file($_FILES['subir']['tmp_name'], "./subidos/{$_FILES['subir']['name']}");

        if ($_FILES['subir']['name'] !=""){
            $xArticulo = simplexml_load_file("./subidos/{$_FILES['subir']['name']}");
            $elArticulo = $xArticulo->XPath("//articulo");

            foreach ($elArticulo as $dades) {
                $atributs = $dades->attributes();
            //    $cCategoria = $atributs['categoria'];
                $cTitulo = $dades->titulo;
                $cTitulo_url = $dades->titulo_url;
            //    $cFoto = $dades->foto;
                $cContenido = $dades->contenido;
            }
            $cFoto="image-not-found.png";

            // ===== FORMULARIO =============== //
?>
            <form method='post' action='index.php?app=subir-articulo' class='formulario elevar_ventana' id='form_subir_art' enctype='multipart/form-data'>
                <div class='articulo_subido'>

                    <div class='marcoCaja container'>
                        <input type='hidden' name='app' value='subir-articulo' />

                        <div class='row'>
                            <div class='col-8 Titulo'>Título: <?php echo $cTitulo; ?></div>
                            <div class='col-4'><input id='cFoto' type='file' class='form-control btn btn-success Foto' name='cFoto' accept='image/*' /></div>
                        </div>

                        <div class='row'>
                            <div class='col-8'>

                                <div class='row'>
                        <!--            <div class='col-12 Categoria'>Categoría: <?php // $cCategoria[0]  ?></div>  -->
                                    <div class='col-12 Categoria'>Categoría: Sin categoría</div>
                                </div>

                                <div class='row'>
                                    <div class='col-12 Titulo_url'>Titulo_url: <?php echo $cTitulo_url; ?> </div>
                                </div>

                                <div class='row'>
                                    <div class='col-12 Foto'>Foto: <?php echo $cFoto; ?> </div>
                                </div>

                                <div class='row'>
                                    <div class='col-12 Contenido'> <?php echo $cContenido; ?> </div>
                                </div>

                            </div>    <!--- Cierrer de <div class='col-8'>  -->

                            <div class='col-4 laFoto'>
                                <br><br><span><img id='imgSalida_lg' src='' alt='Foto'/></span>
                            </div>

                            <input type='submit' name='envio' value='Cancelar' class='btn btn-danger'>
                            <input type='submit' name='envio' value='Enviar datos' class='btn btn-success'>

                        </div>   <!--  Cierre de <div class='row'>  -->
                    </div>       <!-- Cierrer de <div class='marcoCaja container'>  -->
                </div>       <!-- Cierre de <section class='articulo_subido'>  -->
            </form>
<?php
echo "<hr /><pre>";
print_r($_GET);
echo "</pre><hr /><pre>";
print_r($_FILES);
echo "</pre><hr />";

            //if (isset($_FILES['cFoto']['name'])){
            if ($_POST['envio'] == 'Enviar datos'){
                $res = grabar_datos($cTitulo, $cTitulo_url, $cFoto, $cContenido);

                if ( isset($_FILES['cFoto']['name']) ){
                    move_uploaded_file($_FILES['cFoto']['tmp_name'], "../publico/img/articulos/{$_FILES['cFoto']['name']}");
                }

                $cFoto=$_POST['cFoto'];
            }
        }
    }
}
?>
