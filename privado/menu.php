<div class="container">
    <ul class="row" id="menuPrivado">
     <!-- =============== MENU NAVEGACIÓN ================- -->


     <nav class="navbar navbar-expand-lg barraMenu">
        <a class="navbar-brand"><span>[ ZONA PRIVADA ] </span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> <> </span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto text-center">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home | Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?app=ver-categorias">Lista categorías</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?app=ver-articulos">Lista artículos</a>
            </li>
            <li class="nav-item dropdown">
             <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Gestionar Categoría
             </a>
             <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">

               <a class="dropdown-item" href="index.php?app=crear-categoria">Crear nueva categoría</a>
               <a class="dropdown-item" href="index.php?app=eliminar-categoria">Eliminar categoría</a>
             </div>
           </li>

           <li class="nav-item dropdown">
             <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Gestionar Artículo
             </a>
             <div class="dropdown-menu"  aria-labelledby="navbarDropdownMenuLink2">
               <a class="dropdown-item" href="index.php?app=crear-articulo">Crear nuevo artículo</a>
               <a class="dropdown-item" href="index.php?app=modificar-articulo">Modificar artículo</a>
               <a class="dropdown-item" href="index.php?app=eliminar-articulo">Eliminar artículo</a>
               <a class="dropdown-item" href="index.php?app=subir-articulo">Subir artículo (archivo XML)</a>
             </div>
           </li>
         </ul>
       </div>
     </nav>
    </ul>
</div>
