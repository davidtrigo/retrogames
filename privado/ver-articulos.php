<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $('tr').click(function(){
            var texto = $(this).closest('tr').children('td.celda1').text();
            texto = "index.php?app=ver-articulos&ID="+texto;
            location.href= texto;
        });
    });
</script>

<?php
require_once "funciones-comunes.php";

function verArticulos(){
echo <<<form_verArt
    <h3 class="mensaje_central">Lista de artículos</h3>
    <form class="formulario" name="form1" action="" method="post">
        <div class="input-group">
            <span class="input-group-addon"><b>Buscar artículo/s</b></span>
            <input id="cTitulo" type="text" class="form-control" name="cTitulo" value="">
            <input type="submit" value=" Enviar " id="enviar" name="enviar" class="btn btn-success" />
        </div>
    </form>
form_verArt;

    if (isset($_POST['enviar'])){
        $busca = $_POST['cTitulo'];
    } else {
        $busca = "*";
    }
    $lista = buscar_articulos($busca);

    if (count($lista) > 0){
        echo "<div class='formulario'>";
            echo "<p><b>Artículos encontrados: ".count($lista). "</b></p>";
            echo "<div id='form_modificar'>";
                echo "<table class='table table-condensed'>";
                    echo "<tr><th> ID </th> <th> Título </th></tr>";
                    foreach ($lista as $fila) {
                        echo "<tr><td class='celda1'>{$fila['id']}</td> <td>{$fila['titulo']}</td></tr>";
                    }
                echo "</table>";
            echo "</div>";
        echo "</div>";

    }
    if (isset($_GET['ID'])) {
        $idSeleccionado= $_GET['ID'];

        require_once "funciones-comunes.php";

            $datos = buscar_articulo_ID($idSeleccionado);
            $datos_art=$datos[0];

            $datos2 = lista_categorias();
            foreach ($datos2 as $fila) {
                if ($fila['id']==$datos_art['categoria']) {
                    $nombreCat= $fila['nombre'];
                }
            }


        echo <<<codigoHTML
            <div class='formulario '>
                <div class='marcoCaja container'>
                    <input type='hidden' name='app' value='ver-articulos' />
                    <div class='row'>
                        <div class='col-8 Titulo'>Título: {$datos_art['titulo']}</div>
                        <div class='col-4'><div class='col-12 Titulo'>Categoría: {$datos_art['categoria']}</div>
                    </div>

                    <div class='row'>
                        <div class='col-8'>

                            <div class='row'>
                                <div class='col-12 Titulo_url'>Titulo_url: {$datos_art['titulo_url']}</div>
                            </div>

                            <div class='row'>
                                <div class='col-12 Foto'>Foto: {$datos_art['foto']}</div>
                            </div>

                            <div class='row'>
                                <div class='col-12 Contenido'> {$datos_art['contenido']}</div>
                            </div>

                        </div>    <!--- Cierrer de <div class='col-8'>  -->

                        <div class='col-4 imgSalida_lg' style="background-image:url('../publico/img/articulos/{$datos_art['foto']}')">

                        </div>
                    </div>
                </div>
            </div>
codigoHTML;

        }
    }
?>
