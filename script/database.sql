/* ==========================================================*/
/* =========== CREACIÓN BASE DE DATOS Y TABLAS  =============*/
/* ==========================================================*/

CREATE DATABASE IF NOT EXISTS bbdd_retrogames CHARACTER SET utf8 COLLATE utf8_general_ci;
USE bbdd_retrogames;

/* =========== CREACIÓN TABLA CATEGORIA  =============*/
CREATE TABLE categoria(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(150) NOT NULL,
    nombre_url VARCHAR(150) UNIQUE NOT NULL,
    foto VARCHAR(150),
    descripcion VARCHAR(10000)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

/* =========== CREACIÓN TABLA ARTÍCULO  =============*/
CREATE TABLE articulo(
    id INT PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(150) NOT NULL,
    titulo_url VARCHAR(150) UNIQUE NOT NULL,
    foto VARCHAR(150),
    contenido VARCHAR(10000),
    fecha_creacion TIMESTAMP,
    categoria INT,

    FOREIGN KEY(categoria) REFERENCES categoria(id) ON DELETE SET NULL ON UPDATE CASCADE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

/* =========== CREACIÓN TABLA USUARIO  =============*/
CREATE TABLE usuario(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(150) NOT NULL,
    password VARCHAR(150) UNIQUE NOT NULL
) CHARACTER SET utf8 COLLATE utf8_general_ci;


/* =========== INSERCION DE DATOS  =============*/

/*=======CATEGORIA===========*/

INSERT INTO `categoria` (`id`, `nombre`, `nombre_url`, `foto`,`descripcion`) VALUES
(1, 'Deportivos', 'deportivos','deportivos.gif', 'Basados en cualquier clase de deporte: Fútbol americano, fútbol, baloncesto, béisbol, boxeo, etc. En general, la partida se lleva a cabo siguiendo las reglas estándares del deporte aunque se mide por un tiempo corto definido, no más de 10 minutos por lo regular. Pasado este tiempo si el jugador lleva la ventaja podrá seguir jugando. En caso contrario, si el jugador está empatado con la máquina o perdiendo, se considera juego terminado o juego concluido (game over). Por lo general no ofrecen una historia o premisa de juego, pero sí formaciones o cambios de jugadores (en los juegos deportivos actuales).');
INSERT INTO `categoria` (`id`, `nombre`, `nombre_url`, `foto`,`descripcion`) VALUES
(2, 'Lucha', 'lucha','lucha.gif', 'Este juego consiste en enfrentar dos o más personajes por ocasión entre sí. Las partidas pueden involucrar 1, 2 o hasta 3 rondas por combate, o bien, permite usar hasta 4 personajes en una misma partida. Conforme el jugador gana los combates, enfrenta a los siguientes luchadores. Es un género bastante popular y abundante en secuelas. Debido a lo anterior, son especialmente ricos en personajes (podemos encontrar juegos que tienen desde 8 hasta 64 personajes); a su vez, cada personaje tiene aspecto, capacidades, poderes, datos personales y una historia compleja que lo vuelve único. En los últimos años, las historias de cada personaje o grupo de personajes se entrelanzan para hacer una historia común que intenta explicar los enfrentamientos.');
INSERT INTO `categoria` (`id`, `nombre`, `nombre_url`, `foto`,`descripcion`) VALUES
(3, 'Carreras', 'carreras','carreras.gif', 'Juegos ambientados en las competencias de autos, motocicletas, etc. Es frecuente que también posean asientos y mandos con forma de volantes, palanca de cambio de velocidad (en algunos casos la máquina tiene la forma del vehículo). A diferencia de los simuladores, el objetivo primordial es quedar en un lugar aceptable en una competencia (en ocasiones como mínimo en 3.er lugar) o cumplir con un tiempo específico. Por lo general no ofrecen una historia o premisa de juego.');
INSERT INTO `categoria` (`id`, `nombre`, `nombre_url`, `foto`,`descripcion`) VALUES
(4, 'Plataformas', 'plataformas','plataformas.gif','Estos juegos muestran numerosas semejanzas con las arcades de Acción, sin embargo son nombrados así debido a que el personaje debe desplazarse por "planos" o "plataformas" y puede "morir", entre otras causas, por una caída desde una de estas. Estos juegos por lo general son en 2 dimensiones "2D" (un ejemplo claro es Super Mario Bros.) aunque existen raras excepciones en 3 dimensiones para arcades. Otra característica que los diferencia de los juegos de Acción es que solo permiten un jugador por vez. Para estos juegos, es vital poseer una premisa, no obstante, suele ser sumamente sencilla. Dado que poseen una historia, existen algunos personajes.');
INSERT INTO `categoria` (`id`, `nombre`, `nombre_url`, `foto`,`descripcion`) VALUES
(5, 'Shooter', 'shooter','shooter.gif','Llamados así debido a que la acción principal del juego es disparar. Un personaje, vehículo o nave con completa libertad de movimiento se desplaza por toda la pantalla mientras el nivel se despliega lentamente, mostrando obstáculos o enemigos capaces de destruir a dicho vehículo. El jugador evita perder disparando o evadiendo dichos obstáculos. Usualmente es ambientado con aeronaves o naves espaciales, pero los ambientes también pueden incluir: submarinos, el oeste estadounidense, eras cavernarias, etc.');

/*=======ARTICULO===========*/

INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(1, 'Kick-Off', 'kick-off', 'kick-off.jpg', 'Desde el momento de su lanzamiento destacó por una característica que nunca antes había tenido otro simulador de fútbol: el balón no iba pegado al pie del jugador que lo controlaba, sino que este (de manera automática) le daba pequeñas patadas. Esto implicaba que el futbolista que llevaba el balón sólo podía girar cuando el balón estaba muy cerca de su pie (en el momento de darle la patada), en otro caso el jugador giraba pero perdía el control del balón. Así se añadía una cierta dificultad al juego y los jugadores con más entrenamiento disponían de un rango mayor de maniobras que los noveles.', '2018-04-07 22:00:00', 1);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(2, 'Out Run', 'out-run', 'out-run.jpg', 'OutRun fue el primer video juego arcade que permite al usuario elegir la música de fondo. Una banda sonora de música de playa relajado (PASSING BREEZE), muy similar en estilo y tono a los populares grupos de los 70 / 80 japoneses de jazz fusión, y algunos ritmos latino-caribeños tipo Miami Sound Machine (MAGICAL SOUND SHOWER y SPLASH WAVE). Tres pistas seleccionables se presentaron en total y fueron difundidos a través de una imaginaria estación de FM Radio recibida por el receptor del radio en el Testarossa.', '2018-01-07 23:00:00', 3);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(3, 'Street Fighter II', 'street-figther-ii', 'street-figther-ii.png', 'Es un videojuego de lucha producido por Capcom en el año 1991, originalmente publicado como un videojuego arcade. Siendo una secuela del videojuego Street Fighter original, Street Fighter II mejoró los muchos conceptos introducidos en el primer videojuego anterior, incluyendo el uso de movimientos especiales basados en el comando y una configuración de seis botones, mientras que también ofrece a los jugadores una selección de múltiples personajes disponibles, cada uno con su propio estilo de lucha y movimientos especiales únicos.', '2017-12-07 23:00:00', 2);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`)
 VALUES(4, 'The Last Blade', 'the-last-blade', 'the-last-blade.jpg', 'El videojuego tiene lugar durante fines de la era del shogunato Tokugawa en Japón, e incorpora diversos elementos de la mitología japonesa. Como tal, la música de fondo generalmente incorpora instrumentos sintetizados simulando un sonido apropiado para el ajuste del siglo XIX, en un estilo occidental clásico, pseudo-romántico (inusual para un videojuego de lucha). Muchos de los personajes de la serie fueron inspirados intencionalmente en el manga Rurouni Kenshin. Los desarrolladores afirman que se trataba de un gesto amistoso al mangaka Nobuhiro Watsuki por ser un fan muy reconocido de la serie Samurai Shodown.', '2018-05-07 22:00:00', 2);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(5, 'Wolfenstein 3D', 'wolfenstein-3d', 'wolfenstein-3d.jpg', 'En Wolfenstein 3D el jugador es William "B.J." Blazkowicz, un espía estadounidense intentando escapar de la fortaleza nazi en la cual se encuentra prisionero. Esta fortaleza está llena de guardias armados y de perros entrenados para el ataque. El edificio tiene un gran número de cuartos secretos que contienen tesoros, raciones de alimentos y botiquines de primeros auxilios, al igual que diferentes tipos de armas y municiones, todo lo cual ayudará al jugador a lograr su objetivo..', '2018-05-07 22:00:00', 5);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(6, 'Doom', 'doom', 'doom.jpg', 'El juego consiste en comandar a un marine, que se encuentra de misión rutinaria en una estación en Fobos (una de las lunas de Marte), cuando de repente se produce un fallo en un experimento de teleportación que se llevaba a cabo allí, abriéndose así las puertas del infierno y dejando libres a un sinfín de demonios, y espíritus malignos que se apoderan de los cuerpos de los marines caídos, transformándolos en zombis, infestando rápidamente todas las instalaciones. Como protagonista, el jugador es el único ser humano sobreviviente en la estación y su misión es abrirse paso entre los enemigos nivel a nivel.', '2018-05-07 22:00:00', 5);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(7, 'Super Mario Bros', 'super-mario-bros', 'super-mario-bros.png', 'Super Mario Bros. tiene lugar en el pacífico Reino Champiñón, donde viven hongos antropomorfos, que fue invadido por los Koopa, una tribu de tortugas. El tranquilo pueblo es convertido en piedra y ladrillos, y el reino de los champiñones se va a la ruina. La única que puede deshacer el influjo mágico de ellos es la Princesa Peach, hija del Rey Champiñón. Desafortunadamente, está en las garras del Rey Tortuga Koopa, Bowser.', '2018-05-07 22:00:00', 4);
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(8, 'NBA Jam', 'nba-jam', 'nba-jam.jpg', 'NBA Jam supuso todo un cambio en el mundo de los simuladores deportivos, dando un enfoque menos realista y más espectacular, buscando la diversión inmediata.El videojuego consta de partidos de baloncesto 2 contra 2 entre los equipos de la NBA. Aunque en un principio NBA Jam parece un videojuego normal y corriente, pronto nos damos cuenta de que no es así. El juego es totalmente arcade no existiendo ningún tipo de sanción por faltas. Tampoco afectan otro tipo de infracciones como el capo atrás o la zona. Pero lo más espectacular del juego está al encestar: saltos imposibles, mates estratosféricos y tapones altísimos. El juego era todo un espectáculo visual lleno de dinamismo y generando grandes dosis de diversión', '2018-05-05 22:00:00',1 );
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(9, 'R.C.Pro-Am', 'r-c-pro-am', 'r-c-pro-am.jpg', 'Como punto original, R.C. Pro-Am ofrecia el manejo de vehículos de radiocontrol, algo que nos nota a primera vista en el videojuego, si no fuera por la aparición gráfica de antenas receptoras, pero que justifica perfectamente su concepción totalmente arcade.', '2018-05-05 22:00:00',3 );
INSERT INTO `articulo` (`id`, `titulo`, `titulo_url`, `foto`, `contenido`, `fecha_creacion`, `categoria`) VALUES
(10, 'Donkey Kong', 'donkey-kong', 'donkey-kong.jpg', 'Es un primitivo juego del género plataformas que se centra en controlar al personaje sobre una serie de plataformas mientras evita obstáculos. La historia no es muy compleja, pero funcionó en aquella época. El juego consiste en que Mario (en el juego, Jumpman) debe rescatar a una dama que había sido capturada por un enorme mono llamado Donkey Kong. Estos dos personajes se volvieron de los más famosos de Nintendo.', '2018-05-05 22:00:00',4 );


/*==========USUARIO============================*/


INSERT INTO usuario(nombre,password) values('test', SHA2('test',256));
