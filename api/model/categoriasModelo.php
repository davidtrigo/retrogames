<?php

require_once __DIR__ . "/../utils/conexion.php";

class CategoriasModelo extends Conexion {

    function __construct($datosConexion) {
        parent::__construct($datosConexion);
    }

    /**  Metodo que obtiene un listado de categorias
     * @return array
     */
    public function getCategorias() {
        $querycategorias = $this->getConexion()->query("select * from categoria");
        $categorias = $querycategorias->fetchAll(PDO::FETCH_ASSOC);
        return $categorias;
    }

    /** Meotodo que obtiene una categoria a partir del id pasado por parametro
     * @param $id
     * @return array
     */
    public function getCategoria($id) {
        $consulta = [];
        //Buscar las categorías por su id
        $querycategoria = $this->getConexion()->query("select * from categoria where id =$id");
        $categoria = $querycategoria->fetchAll(PDO::FETCH_ASSOC);
        //Buscar los artículos que pertenecen a una categoria.
        $queryarticulo = $this->getConexion()->query("select articulo.id from articulo, categoria where articulo.categoria=categoria.id and articulo.categoria=$id");
        $articulo = $queryarticulo->fetchAll(PDO::FETCH_ASSOC);
        $consulta = $categoria;
        $consulta['articulo'] = $articulo;
        return $consulta;
    }
}
?>