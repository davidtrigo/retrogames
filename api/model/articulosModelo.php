<?php
require_once __DIR__ . "/../utils/conexion.php";

class ArticulosModelo extends conexion
{
    /**
     * ArticulosModelo constructor.
     * @param $datosConexion
     */
    function __construct($datosConexion)
    {
        parent::__construct($datosConexion);
    }

    /** Metodo que obtiene un listado de articulos a partir de los parametros pasados
     * @param $fechaMax  fecha maxima a buscar
     * @param $fechaMin  fecha minima a buscar
     * @param $ultArticulos   muestra los n ultimos articulos.
     * @return array
     */
    public function getArticulos($fechaMax, $fechaMin, $ultArticulos)
    {

        //  if ($fechaMin == null) {$fechaMin=date("d-m-Y",1);} else {$fechaMin  =date("Y-m-d",strtotime($fechaMin));}
        // if ($fechaMax == null) {$fechaMax=date("d-m-Y");}else {$fechaMax  =date("Y-m-d",strtotime($fechaMax));}

            if (($fechaMin != null) && ($fechaMax != null) ) {

                 if(($ultArticulos != null)){
                     $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion between '$fechaMin' and '$fechaMax' order by fecha_creacion desc limit $ultArticulos;");
                 } else {
                     $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion between '$fechaMin' and '$fechaMax'");
                 }
               return $queryArticulos->fetchAll(PDO::FECTH_ASSOC);
             }



        if (($fechaMin == null) && ($fechaMax != null)) {
            $fechaMin= date("Y-m-d",1);

            $fechaMax = date("Y-m-d", strtotime($fechaMax));

            if (($ultArticulos != null)) {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion  between '$fechaMin' and '$fechaMax' order by fecha_creacion desc limit $ultArticulos;");
            } else {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion between '$fechaMin' and '$fechaMax'");
            }
            return $queryArticulos->fetchAll(PDO::FETCH_ASSOC);
        }


        if (($fechaMin != null) && ($fechaMax == null)) {
            $fechaMax = date("Y-m-d");
            $fechaMin = date("Y-m-d", strtotime($fechaMin));

            if (($ultArticulos != null)) {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion  between '$fechaMin' and '$fechaMax' order by fecha_creacion desc limit $ultArticulos;");
            } else {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion between '$fechaMin' and '$fechaMax'");
            }
            return $queryArticulos->fetchAll(PDO::FETCH_ASSOC);
        }

        if (($fechaMax == null) && ($fechaMin == null)) {
            if (($ultArticulos != null)) {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo where fecha_creacion order by  fecha_creacion desc limit $ultArticulos;");
            } else {
                $queryArticulos = $this->getConexion()->query("SELECT * FROM articulo");
            }
            return $queryArticulos->fetchAll(PDO::FETCH_ASSOC);
        }


    }

    /** Obtiene un articulo a partir del id pasado por parametro
     * @param $id  id  de un articulo
     * @return array|mixed
     */
    public function getArticulo($id)
    {

        /**select date_format(now(), '%d/%m/%Y')*/
        $consulta = [];
        $queryArticulo = $this->getConexion()->query("select id, titulo, titulo_url, foto, contenido, date_format(fecha_creacion,'%d-%m-%Y') as fecha_creacion from articulo WHERE id =  $id ;");
        $articulo = $queryArticulo->fetch(PDO::FETCH_ASSOC);
        $queryCategoria = $this->getConexion()->query("SELECT categoria.id FROM articulo, categoria WHERE articulo.categoria = categoria.id and articulo.id = $id;");
        $categoria = [];
        if ($queryCategoria !== false && $queryCategoria->rowCount() > 0) $categoria = $queryCategoria->fetchAll(PDO::FETCH_ASSOC)[0];
        $consulta = $articulo;
        $consulta["categoria"] = $categoria;

        return $consulta;
    }
}

?>