<?php
require_once 'vendor/autoload.php';
require_once 'model/categoriasModelo.php';
require_once 'model/articulosModelo.php';
require_once "controller/categoriasControl.php";
require_once 'controller/articulosControl.php';
require_once 'controller/documentoControl.php';
require_once 'utils/conexion.php';


$configuration = ['settings' => ['displayErrorDetails' => true, 'addContentLengthHeader' => false]];

$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
/*Local*/


$db = [
    'host' => 'localhost',
    'dbname' => 'bbdd_retrogames',
    'user' => 'root',
    'password' => ''
];
/*remoto*/
/*
$db = [
    'host' => 'retrogames.mywebcommunity.org',
    'dbname' => '3225151_retrogames',
    'user' => '3225151_retrogames',
    'password' => '6RkbTl9v8hXksqg'
];
*/
$c['categorias'] = function () {
    global $db;
    $categoriaModelo = new CategoriasModelo($db);
    return $categoriaModelo;
};

$c['articulos'] = function() {
    global $db;
    $articuloModelo = new ArticulosModelo($db);
    return $articuloModelo;
};

//rutas
$app->get("/categorias","\categoriasControl:getCategorias");
$app->get("/categorias/{id}", "\categoriasControl:getCategoria");
$app->get("/articulos", "\ArticulosControl:getArticulos");
$app->get("/articulos/{id}", "ArticulosControl:getArticulo");
$app->get("/doc", "DocumentoControl:getDocumento");
$app->run();
?>