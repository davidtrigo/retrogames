<?php

require_once __DIR__ . "/../utils/conexion.php";

class ArticulosControl
{
    private $c;

    function __construct($container)
    {
        $this->c = $container;
    }

    /** Metodo que obtiene un listado de articulos junto a su categoria
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function getArticulos($request, $response, $args)
    {
        $fechaMax = null;
        $fechaMin = null;
        $ultArticulos = null;
        $params = $request->getQueryParams();

        if (isset($params['fechaMax'])) {
            $fechaMax = $params['fechaMax'];
        }
        if (isset($params['fechaMin'])) {
            $fechaMin = $params['fechaMin'];
        }
        if (isset($params['ultArticulos'])) {
            $ultArticulos = $params['ultArticulos'];
        }

        $articulos = $this->c->articulos->getArticulos($fechaMax, $fechaMin, $ultArticulos);
        $resultadoUrl = [];
        if ($articulos == null) {
            
            $fecha1 = $params['fechaMin'];
            $fecha2 = date("d-m-Y");
            if ($fecha1 < $fecha2) {
                $resultadoUrl = ["ERROR => La fecha minima $fecha1 no puede ser mayor que la fecha de hoy ($fecha2)"];
            } else {
                $resultadoUrl = ["ERROR => No se ha devuelto resultados, revisa que el formato de fecha sea d-m-Y "];
            }

        } else {
            foreach ($articulos as $articulo) {
                $id = $articulo['id'];
                $resultadoUrl[] = $this->listadoCategoria($id);
            }
        }
        $response = $response->withJson($resultadoUrl);
        return $response;

    }


    /** Método que lista la categoria de cada articulo pasando por parametro el id del articulo
     * @param  $id  identificador del articulo
     * @return mixed retorna un array de enlaces de categorias.
     */
    private function listadoCategoria($id)
    {
        $articulo = $this->c->articulos->getArticulo($id);
        $resultadoUrl = [];
        foreach ($articulo['categoria'] as $categoria) {
            $resultadoUrl[] = $this->c->get("request")->getUri()->getScheme() . "://" . $this->c->get('request')->getUri()->getHost() .
                $this->c->get('request')->getUri()->getBasePath() . "/categorias/" . $categoria;
        }
        $articulo['categoria'] = $resultadoUrl;
        return $articulo;
    }

    /**  Método que obtiene un aticulo a partir del id pasado por $args
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function getArticulo($request, $response, $args)
    {
        $id = $args['id'];
        $articulo = $this->c->articulos->getArticulo($id);
        $resultadoUrl = [];

        foreach ($articulo['categoria'] as $categoria) {
            $resultadoUrl[] = $request->getUri()->getScheme() . "://" . $request->getUri()->getHost() . $request->getUri()->getBasePath() . "/categorias/" . $categoria;
        }

        $articulo["categoria"] = $resultadoUrl;
        $response = $response->withJson($articulo);
        return $response;

    }
}