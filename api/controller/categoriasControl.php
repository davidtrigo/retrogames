<?php

require_once __DIR__ . "/../utils/conexion.php";

class CategoriasControl {

    private $c;

    function __construct($container) {
        $this->c = $container;
    }

    /** Metodo que obtiene un listado de categorias junto a sus articulos
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function getCategorias($request, $response, $args) {
        $categorias = $this->c->categorias->getCategorias();
        $resultadoUrl = [];
        //Bucle para agregar los artículos a cada categoría
        foreach ($categorias as $categoria) {
            $id = $categoria['id'];
            $resultadoUrl[] = $this->listadoArticulos($id);
        }
        $response = $response->withJson($resultadoUrl);
        return $response;
    }

    /*** Método que lista  el articulo de cada categoria pasando por parametro el id de categoria
     * @param $id identificador de categoria
     * @return mixed
     */
    private function listadoArticulos($id) {

        $categoria = $this->c->categorias->getCategoria($id);
        $url = [];
        foreach ($categoria['articulo'] as $articulo) {
            $url[] = $this->c->get("request")->getUri()->getScheme() . "://" . $this->c->get('request')->getUri()->getHost() .
                $this->c->get('request')->getUri()->getBasePath() . "/articulos/" . $articulo['id'];
        }
        $categoria['articulo'] = $url;
        return $categoria;
    }

    /**  Método que obtiene una categoria a partir del id pasado por $args
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function getCategoria($request, $response, $args) {
        $id = $args['id'];
        $categoria = $this->c->categorias->getCategoria($id);
        //Agregar los artículos que pertenecen a cada categoría en un array
        $resultadoUrl = [];
        foreach ($categoria['articulo'] as $articulo) {
            $resultadoUrl[] = $request->getUri()->getScheme() . "://" . $request->getUri()->getHost() . $request->getUri()->getBasePath() . "/articulos/" . $articulo['id'];
        }
        $categoria['articulo'] = $resultadoUrl;
        $response = $response->withJson($categoria);
        return $response;
    }

}