<?php
class conexion{
    private $_conection;


   public function __construct($datosConexion)
   // public function __construct()
    {$this->_conection = new PDO("mysql:host={$datosConexion['host']}; dbname={$datosConexion['dbname']};charset=utf8", $datosConexion['user'], $datosConexion['password']);
   // $this->_conection = new PDO("mysql:host=localhost; dbname= bbdd_retrogames;charset=utf8", "root","");
        $this->_conection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getConexion() {
        return $this->_conection;
    }

}

?>